# See https://docs.getchef.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "vishnu"
client_key               "#{current_dir}/vishnu.pem"
chef_server_url          "https://chefserver.poc.local/organizations/jenkins_chef_poc"
cookbook_path            ["#{current_dir}/../"]
validation_client_name   "jenkins_chef_poc-validator"
validation_key           "#{current_dir}/jenkins_chef_poc-validator.pem"
